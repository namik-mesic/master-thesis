<?php

use Faker\Generator as Faker;
use App\User;
use App\Role;
use App\Article;

$factory->define(User::class, function(Faker $fake) {

    return [
        'name' => $fake->name,
        'email' => $fake->email,
        'password' => bcrypt(str_random(10))
    ];

});

$factory->define(Article::class, function(Faker $fake) {

    return [
        'title' => $fake->title,
        'content' => $fake->text
    ];

});

$factory->define(Role::class, function(Faker $fake) {

    return [
        'name' => $fake->word,
        'display_name' => $fake->name,
        'description' => $fake->text
    ];

});
