<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Client;
use App\User;
use App\WorkOrder;
use App\WorkOrderDocument;
use App\Supplier;
use App\WorkOrderNote;
use App\EndCustomer;
use App\Material;
use App\Product;
use App\Offer;

class DatabaseSeeder extends Seeder
{
    protected $schema;

    protected $protectedTables = [
        'migrations'
    ];

    protected $protectedTablesString;

    public function __construct()
    {
        $this->schema = env('DB_DATABASE', 'master');
        $this->protectedTablesString = implode(', ', $this->protectedTables);
    }

    protected function clearTables()
    {

        $records = \DB::select("
            SELECT table_name FROM information_schema.tables
            WHERE table_schema = '{$this->schema}'
            AND table_name NOT IN ('{$this->protectedTablesString}')
        ");

        foreach ($records as $record)
            \DB::table($record->table_name)->truncate();
    }

    public function run()
    {
        Model::unguard();
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->clearTables();

        $this->call(EntrustSeeder::class);
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Model::unguard(false);
    }
}