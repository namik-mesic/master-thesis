<?php

use App\Role;
use App\Permission;
use App\User;

class EntrustSeeder extends DatabaseSeeder
{
    public function run()
    {
        $admin = Role::create([
            'author_id' => 1,
            'id' => 1,
            'name' => 'admin',
            'display_name' => 'Administrator',
            'description' => 'Can do everything'
        ]);

        $v1Permission = Permission::create([
            'author_id' => 1,
            'id' => 1,
            'name' => 'v1',
            'display_name' => 'v1 API',
            'description' => 'Allows the usage of the entire v1 API'
        ]);

        $admin->attachPermissions([
            $v1Permission
        ]);

        $defaultUser = User::create([
            'id' => 1,
            'name' => 'John Doe',
            'email' => 'john.doe@gmail.com',
            'password' => 'password'
        ]);

        $defaultUser->attachRoles([
            $admin
        ]);
    }
}