<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedInteger('author_id')->nullable();
            $table->foreign('author_id')->references('id')->on('users');

            $table->string('title');
            $table->longText('content');

            $table->string('image_file_name');
            $table->integer('image_file_size');
            $table->string('image_content_type');
            $table->timestamp('image_updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }
}
