<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PointerBa\Bundle\AuthoredTrait;
use PointerBa\Bundle\CacheObserver;
use Zizaco\Entrust\Contracts\EntrustRoleInterface;
use Zizaco\Entrust\Traits\EntrustRoleTrait;

class Role extends Model implements EntrustRoleInterface
{
    use EntrustRoleTrait;
    use AuthoredTrait;

    const ADMIN = 1;

    protected $table = 'roles';

    protected $fillable = [
        'name',
        'display_name',
        'description'
    ];

    public static function boot()
    {
        parent::boot();

        static::observe(CacheObserver::class);
    }

    public static function getAdminRole()
    {
        return static::find(static::ADMIN);
    }
}
