<?php

namespace App\Repositories;

use App\Permission;

/**
 * Class PermissionRepository
 * @package App\Repositories
 */
class PermissionRepository extends Repository
{
    protected $modelClass = Permission::class;
}