<?php

namespace App\Repositories;

use App\Role;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RoleRepository
 * @package App\Repositories
 */
class RoleRepository extends Repository
{
    protected $modelClass = Role::class;

    protected function doCreate(Model $instance, array $data)
    {
        $instance = parent::doCreate($instance, $data);

        if ($perms = array_get($data, 'permission_id'))
            $instance->perms()->attach($perms);

        return $instance;
    }

    protected function doUpdate(Model $instance, array $data)
    {
        $instance = parent::doUpdate($instance, $data);

        if ($perms = array_get($data, 'permission_id'))
            $instance->perms()->attach($perms);

        return $instance;
    }
}