<?php

namespace App\Repositories;

use Closure;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;

/**
 * @method Collection getAll()
 */
abstract class Repository
{
    /**
     * Base path for repository cache key holder files
     */
    const KEY_STORE_PATH = '../storage/repositories/';

    /**
     * True if only visible results should be returned (logic in onlyVisible())
     *
     * @var bool
     */
    protected $onlyVisible = true;

    /**
     * True if only results of the currently authenticated user should be returned
     *
     * @var bool
     */
    protected $onlyAuthored = false;

    /**
     * Path of repository cache key for a specific repository
     *
     * @var string
     */
    protected $keyStorePath;

    /**
     * Repository class basename
     *
     * @var string
     */
    protected $baseName;

    /**
     * Full class name of model
     *
     * @var string
     */
    protected $modelClass;

    /**
     * True if results should be paginated
     *
     * @var bool
     */
    protected $paginate = false;

    /**
     * Set if the result should be a key-value pair
     *
     * @var array | null
     */
    protected  $asList = null;

    /**
     * Number of result items if result should be paginated
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * True if cache should be used
     *
     * @var bool
     */
    protected $cache = false;

    /**
     * Key under which the result set should be stored
     *
     * @var string
     */
    protected $cacheKey;

    /**
     * Eloquent model instance
     *
     * @var Model
     */
    private $model;

    /**
     * Eloquent builder instance
     *
     * @var Builder
     */
    private $query;

    /**
     * Table name of model
     *
     * @var string
     */
    protected $table;

    /**
     * Cache time in minutes, null represents infinity
     *
     * @var int|null
     */
    protected $cacheTime = null;

    /**
     * Optional extra querying for non-custom queries
     *
     * @var Closure|null
     */
    private $closure = null;

    /**
     * Maximum number of query results, null represents infinity
     *
     * @var int|null
     */
    private $limit = null;

    /**
     * Filter array for the current query
     *
     * @var array
     */
    private $scopes = [];

    /**
     * User when custom closures are to generate a new unique key
     *
     * @var string|null
     */
    private $keyFragment = null;

    /**
     * The identifier of the table
     *
     * @var string
     */
    private $identifier = 'id';

    /**
     * True if the results of this query should be excluded from future queries
     *
     * @var bool
     */
    protected $appendExcludes = false;

    /**
     * Array of id values that should not be in the result set
     *
     * @var array
     */
    protected $excludes = [];

    /**
     * Sets local values
     */
    public function __construct()
    {
        $this->baseName = class_basename($this);
        $this->keyStorePath = static::KEY_STORE_PATH . $this->baseName;
        $this->model = (new $this->modelClass);
        $this->table = $this->model->getTable();
        $this->makeQuery();
    }

    /**
     * Makes cache key
     *
     * @param $body string
     */
    protected function makeCacheKey($body)
    {
        $this->makeCacheKeyBase();

        $this->cacheKey .= $body;

        $this->appendCacheKeyTail();
    }

    /**
     * Sets the base of the cache key
     */
    protected function makeCacheKeyBase()
    {
        $this->cacheKey = ($this->onlyVisible ? '-' : '+')
            . ($this->onlyAuthored ? ($user = \Auth::user()) ? $user->id : 'null' : '+')
            . $this->baseName;
    }

    /**
     * Sets the tail of the cache key
     */
    protected function appendCacheKeyTail()
    {
        $this->cacheKey .= "{" . serialize($this->scopes) . "}<" . implode(',', $this->excludes) . '>';

        if ($this->limit)
            $this->cacheKey .= "|{$this->limit}|";

        if ($this->paginate)
            $this->cacheKey .= "p=" . (Paginator::resolveCurrentPage() ?: 1) . "&pp={$this->perPage}";

        if ($this->closure)
            $this->cacheKey .= $this->keyFragment;
    }

    /**
     * Clears repository cache and empties the cache key store file
     */
    public static function clearCache()
    {
        $path = static::KEY_STORE_PATH . class_basename(get_called_class());

        if (!\File::exists($path))
            return;

        foreach (file($path) as $key)
            \Cache::forget(trim($key));

        \File::put($path, '');
    }

    /**
     * Querying applied before every single query
     *
     * @param Builder $query
     */
    protected function before(Builder $query) {}

    /**
     * Refreshes query state
     */
    protected function makeQuery()
    {
        $this->query = $this->model->newQuery();
        $this->query->select($this->table . '.*');
    }

    /**
     * Querying applied if $onlyVisible is true, expected to be implemented in derived classes
     *
     * @param Builder $query
     */
    protected function scopeVisible(Builder $query) {}

    /**
     * Querying applied if $onlyAuthored is true, expected to be implemented in derived classes
     *
     * @param Builder $query
     */
    protected function scopeAuthored(Builder $query)
    {
        $query->where('author_id', '=', \Auth::user()->id);
    }

    /**
     * Querying applied based on the provided scopes
     *
     * @param Builder $query
     * @param array $scopes
     */
    protected function scope(Builder $query, array $scopes)
    {
        if (array_get($scopes, 'withTrashed'))
            $query->withTrashed();

        if (array_get($scopes, 'withAuthor'))
            $query->with('author');
    }

    /**
     * Fetches all results sets with applied scopes and before() scoping
     *
     * @param Builder $query
     */
    protected function all(Builder $query) {}

    /**
     * Sets the closure for addition custom querying and the related key fragment
     *
     * @param Closure $closure
     * @param $keyFragment
     * @return $this
     */
    public function setClosure(Closure $closure, $keyFragment)
    {
        $this->closure = $closure;
        $this->keyFragment = $keyFragment;

        return $this;
    }

    /**
     * Stores given data to cache and adjusts repository key files
     *
     * @param $data
     */
    private function storeToCache($data)
    {
        if ($data === null)
            $data = 'null';

        $this->cacheTime > 0
            ? \Cache::put($this->cacheKey, $data, $this->cacheTime)
            : \Cache::forever($this->cacheKey, $data);

        !\File::exists($this->keyStorePath)
            ? \File::put($this->keyStorePath, $this->cacheKey . "\n")
            : \File::append($this->keyStorePath, $this->cacheKey . "\n");
    }

    /**
     * Returns data and stores to cache if needed as well as prepares a new querying environment
     *
     * @param $data
     * @param bool|false $storeToCache
     * @return null
     */
    protected function retrieve($data, $storeToCache = false)
    {
        $this->scopes = [];

        if ($this->appendExcludes)
        {
            $append = $data instanceof Collection
                ? $data->lists($this->identifier)
                    ->toArray()
                : [$data->{$this->identifier}];

            $this->excludes += $append;

            $this->appendExcludes = false;
        }

        if ($storeToCache)
            $this->storeToCache($data);

        $this->makeQuery();

        if ($data === 'null')
            return null;

        return $this->asList && $data instanceof Collection
            ? $data->lists($this->asList[0], $this->asList[1])
                ->toArray()
            : $data;
    }

    /**
     * Applies the before() method to the query and any scopes available
     */
    protected function applyScopes()
    {
        $this->query = $this->query->whereNotIn($this->table . '.' . $this->identifier, $this->excludes);

        $this->before($this->query);
        $this->scope($this->query, $this->scopes);

        if ($this->onlyAuthored)
            $this->scopeAuthored($this->query);

        if ($this->onlyVisible)
            $this->scopeVisible($this->query);

        if ($this->limit)
            $this->query->take($this->limit);

        if ($this->closure)
        {
            $closure = $this->closure;
            $closure($this->query);
        }
    }

    /**
     * Executes the query with all scopes and closures applied
     *
     * @return Collection
     */
    protected function get()
    {
        return $this->paginate
            ? $this->query->paginate($this->perPage)
            : $this->query->get();
    }

    public static function idArray()
    {
        return (new static)
            ->getAll()
            ->lists('id')
            ->toArray();
    }

    /**
     * Applies all local and general scopes to a query and fetches an Eloquent Collection set
     * Methods are defined in the derived class starting with the 'get' keyword and an appropriate method without
     * the get keyword must exist within the derived class
     *
     * @param $method
     * @param $parameters
     * @return Collection|null
     * @throws Exception
     */
    public function __call($method, $parameters)
    {
        if (strlen($method) < 3 || substr($method, 0, 3) !== 'get')
            throw new Exception("Invalid repository method call. Query methods must begin with 'get'.");

        $realMethod = lcfirst(substr($method, 3));

        if (!method_exists($this, $realMethod))
            throw new Exception("Method {$method} not supported by '{$this->baseName}' repository");

        if ($this->cache)
        {
            $this->makeCacheKey(".{$realMethod}[" . serialize($parameters) . "]");

            if ($cache = \Cache::get($this->cacheKey))
                return $this->retrieve($cache);
        }

        $this->applyScopes();

        array_unshift($parameters, $this->query);

        call_user_func_array(array($this, $realMethod), $parameters);

        return $this->retrieve($this->get(), $this->cache);
    }

    /**
     * Applies scopes and finds a single record from the database
     *
     * @param $id
     * @param bool|false $orFail
     * @return Model|null
     */
    public function find($id, $orFail = false)
    {
        if ($this->cache)
        {
            $this->makeCacheKey(".find[{$id}]");

            if ($cache = \Cache::get($this->cacheKey))
            {
                if ($cache === 'null' && $orFail)
                    throw new ModelNotFoundException;

                return $this->retrieve($cache);
            }
        }

        $this->applyScopes();

        $this->query->where("{$this->table}.{$this->identifier}", '=', $id);

        $data = $this->query->first();

        $result = $this->retrieve($data, $this->cache);

        if ($data === null && $orFail)
            throw new ModelNotFoundException;

        return $result;
    }

    /**
     * Simply does the find method and passes the $orFail flag as true
     *
     * @param $id
     * @return Model|null
     */
    public function findOrFail($id)
    {
        return $this->find($id, true);
    }

    /**
     * Queries the database for a single result without any scopes but worries about cache
     *
     * @param Closure $closure
     * @param $cacheKey
     * @param bool|false $orFail
     * @return Model
     */
    public function customFind(Closure $closure, $cacheKey, $orFail = false)
    {
        if ($this->cache)
        {
            $this->cacheKey = "~[{$cacheKey}]";

            if ($cache = \Cache::get($this->cacheKey));
            {
                if ($orFail)
                    throw new ModelNotFoundException;

                return $this->retrieve($cache);
            }
        }

        $closure($this->query);

        $data = $this->query->first();

        $result = $this->retrieve($data, $this->cache);

        if ($data === null && $orFail)
            throw new ModelNotFoundException;

        return $result;
    }

    /**
     * Simply does the customFind method and passes the $orFail flag as true
     *
     * @param Closure $closure
     * @param $cacheKey
     * @return Model
     */
    public function customFindOrFail(Closure $closure, $cacheKey)
    {
        return $this->customFind($closure, $cacheKey, true);
    }

    /**
     * @param Closure $closure
     * @param $cacheKey
     * @return null
     */
    public function customGet(Closure $closure, $cacheKey)
    {
        if ($this->cache)
        {
            $this->cacheKey = "~[{$cacheKey}]";

            if ($cache = \Cache::get($this->cacheKey))
                return $this->retrieve($cache);
        }

        $closure($this->query);

        return $this->retrieve($this->query->get(), $this->cache);
    }

    /**
     * Performs the actual delete, expected to be potentially reimplemented in derived classes
     *
     * @param Model $instance
     * @return bool|null
     * @throws Exception
     */
    protected function doDestroy(Model $instance)
    {
        return $instance->delete();
    }

    /**
     * Deletes a provided resource or resource identifier
     *
     * @param $id Model|int
     * @param bool $orFail
     * @return bool|null
     */
    public function destroy($id, $orFail = false)
    {
        if (!$id instanceof Model)
            $id = $this->find($id, $orFail);

        return $this->doDestroy($id);
    }

    /**
     * Simply calls the destroy method and sets the $orFail flag to true
     *
     * @param $id
     * @return bool|null
     */
    public function destroyOrFail($id)
    {
        return $this->destroy($id, true);
    }

    /**
     * If a Request object is passed to the repository, it will assume the purpose
     * is to fetch all fillable data from the input
     *
     * @param Request $request
     * @return array
     */
    protected function extractData(Request $request)
    {
        return $request->only($this->model->getFillable());
    }

    /**
     * Performs the actual update, expected to be potentially reimplemented in derived classes
     *
     * @param Model $model
     * @param array $data
     * @return bool|int
     */
    protected function doUpdate(Model $model, array $data)
    {
        return $model->fill($data)
            ->save();
    }

    /**
     * Values that can be used on every update, expected to be implemented in derived classes
     *
     * @return array
     */
    protected function updateDefaults()
    {
        return [];
    }

    /**
     * Updates a provided resource or resource identifier based on data / request provided
     *
     * @param $id Model|int
     * @param $data Request|array
     * @param bool|false $orFail
     * @return bool|int
     */
    public function update($id, $data, $orFail = false)
    {
        if (!$id instanceof Model)
            $id = $this->find($id, $orFail);

        if (!$id)
            return null;

        if (!is_array($data))
            $data = $this->extractData($data);

        foreach ($this->updateDefaults() as $key => $value)
            $id->{$key} = $value;

        return $this->doUpdate($id, $data);
    }

    /**
     * Simply does the update method and passes the $orFail flag as true
     *
     * @param $id
     * @param $data
     * @return bool|int
     */
    public function updateOrFail($id, $data)
    {
        return $this->update($id, $data, true);
    }

    /**
     * Performs the actual create, expected to be potentially reimplemented in derived classes
     *
     * @param Model $model
     * @param array $data
     * @return Model
     */
    protected function doCreate(Model $model, array $data)
    {
        $model->fill($data)
            ->save();

        return $model;
    }

    /**
     * Values that can be used on every create, expected to be implemented in derived classes
     *
     * @return array
     */
    protected function createDefaults()
    {
        return [
            'author_id' => ($user = \Auth::user()) ? $user->id : null
        ];
    }

    /**
     * Creates a new resource based on data / request provided
     *
     * @param $data
     * @return Model
     */
    public function create($data)
    {
        if (!is_array($data))
            $data = $this->extractData($data);

        foreach ($this->createDefaults() as $key => $value)
            $this->model->{$key} = $value;

        $instance = $this->doCreate($this->model, $data);

        $this->model = (new $this->modelClass);

        return $instance;
    }

    /**
     * @param $limit int
     * @return $this
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param $cacheTime int
     * @return $this
     */
    public function setCacheTime($cacheTime)
    {
        $this->cacheTime = $cacheTime;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCacheTime()
    {
        return $this->cacheTime;
    }

    /**
     * @param $cache bool
     * @return $this
     */
    public function cache($cache = true)
    {
        $this->cache = $cache;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCaching()
    {
        return $this->cache;
    }

    /**
     * @param $name
     * @param $key
     * @return $this
     */
    public function asList($name, $key)
    {
        $this->asList = [$name, $key];

        return $this;
    }

    /**
     * @param $appendExcludes bool
     * @return $this
     */
    public function appendExcludes($appendExcludes)
    {
        $this->appendExcludes = $appendExcludes;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAppendingExcludes()
    {
        return $this->appendExcludes;
    }

    /**
     * @param array $excludes
     * @return $this
     */
    public function addExcludes(array $excludes)
    {
        $this->excludes += $excludes;

        return $this;
    }

    /**
     * @return array
     */
    public function getExcludes()
    {
        return $this->excludes;
    }

    /**
     * @param array $scopes
     * @return $this
     */
    public function setScopes(array $scopes)
    {
        $this->scopes = $scopes;

        return $this;
    }

    /**
     * @return array
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @param $paginate bool
     * @return $this
     */
    public function paginate($paginate = true)
    {
        $this->paginate = $paginate;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPaginating()
    {
        return $this->paginate;
    }

    /**
     * @param $perPage int
     * @return $this
     */
    public function setPerPage($perPage)
    {
        $this->perPage = $perPage;

        return $this;
    }

    /**
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * @return $this
     */
    public function clearLimit()
    {
        $this->limit = null;

        return $this;
    }

    /**
     * @return $this
     */
    public function clearExcludes()
    {
        $this->excludes = [];

        return $this;
    }

    /**
     * @return $this
     */
    public function clearScopes()
    {
        $this->scopes = [];

        return $this;
    }

    /**
     * @return string
     */
    public function lastCacheKey()
    {
        return $this->cacheKey;
    }

    public function authored($authored = true)
    {
        $this->onlyAuthored = $authored;

        return $this;
    }

    public function visible($visible = true)
    {
        $this->onlyVisible = $visible;

        return $this;
    }
}