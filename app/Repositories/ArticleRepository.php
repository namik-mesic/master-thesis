<?php

namespace App\Repositories;

use App\Article;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ArticleRepository
 * @package App\Repositories
 *
 * @method Collection getByUser(User $user)
 */
class ArticleRepository extends Repository
{
    protected $modelClass = Article::class;

    protected function byUser(Builder $query, User $user)
    {
        $query->where('author_id', '=', $user->id);
    }

    protected function scope(Builder $query, array $scopes)
    {
        parent::scope($query, $scopes);

        if ($title = array_get($scopes, 'title'))
            $query->where('title', 'LIKE', "%{$title}%");
    }
}