<?php

namespace App\Repositories;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ArticleRepository
 * @package App\Repositories
 */
class UserRepository extends Repository
{
    protected $modelClass = User::class;

    protected function doCreate(Model $instance, array $data)
    {
        $instance = parent::doCreate($instance, $data);

        if ($roles = array_get($data, 'role_id'))
            $instance->roles()->attach($roles);

        return $instance;
    }

    protected function doUpdate(Model $instance, array $data)
    {
        $instance = parent::doUpdate($instance, $data);

        if ($roles = array_get($data, 'role_id'))
            $instance->roles()->attach($roles);

        return $instance;
    }
}