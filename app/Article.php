<?php

namespace App;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Database\Eloquent\Model;
use PointerBa\Bundle\AuthoredTrait;
use PointerBa\Bundle\CacheObserver;
use Codesleeve\Stapler\ORM\EloquentTrait as Staplerable;

class Article extends Model implements StaplerableInterface
{
    use AuthoredTrait;
    use Staplerable;

    protected $table = 'articles';

    protected $fillable = [
        'title',
        'content',
        'image'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->hasAttachedFile('image', [
            'default_url' => '/article/:style/missing.jpg'
        ]);
    }

    public static function boot()
    {
        parent::boot();
        static::bootStapler();

        static::observe(CacheObserver::class);
    }
}