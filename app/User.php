<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use PointerBa\Bundle\AuthoredTrait;
use PointerBa\Bundle\CacheObserver;
use Zizaco\Entrust\Contracts\EntrustUserInterface;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Model implements AuthenticatableContract, EntrustUserInterface
{
    use Authenticatable;
    use EntrustUserTrait;
    use AuthoredTrait;

    const DEFAULT_ADMIN = 1;

    protected $table = 'users';

    protected $fillable = [
        'name',
        'email',
        'password'
    ];

    protected $hidden = [
        'password'
    ];

    public static function boot()
    {
        parent::boot();

        static::observe(CacheObserver::class);
    }

    public static function getDefaultAdmin()
    {
        return static::find(static::DEFAULT_ADMIN);
    }

    /*
     * Relations
     */
    public function articles()
    {
        return $this->hasMany(Article::class, 'author_id');
    }

    /*
     * Mutators
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = $password ? bcrypt($password) : null;
    }
}
