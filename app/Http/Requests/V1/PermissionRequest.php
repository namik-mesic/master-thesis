<?php

namespace App\Http\Requests\V1;

use App\Http\Requests\Request;

class PermissionRequest extends Request
{
    public function rules()
    {
        return [
            'name' => 'required|unique:permissions,name'
        ];
    }
}