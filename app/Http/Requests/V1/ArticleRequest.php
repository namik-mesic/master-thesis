<?php

namespace App\Http\Requests\V1;

use App\Http\Requests\Request;

class ArticleRequest extends Request
{
    public function rules()
    {
        return [
            'title' => 'required',
            'content' => 'required',
            'image' => 'image|required'
        ];
    }
}