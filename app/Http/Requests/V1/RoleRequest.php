<?php

namespace App\Http\Requests\V1;

use App\Http\Requests\Request;

class RoleRequest extends Request
{
    public function rules()
    {
        return [
            'name' => 'required|unique:roles,name'
        ];
    }
}