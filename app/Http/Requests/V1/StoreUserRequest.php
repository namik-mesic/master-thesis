<?php

namespace App\Http\Requests\V1;

class StoreUserRequest extends UserRequest
{
    public function rules()
    {
        $rules = parent::rules();

        $rules['email'] .= '|required|unique:users,email';
        $rules['password'] = 'required';

        return $rules;
    }
}