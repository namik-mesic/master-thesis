<?php

namespace App\Http\Requests\V1;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'email'
        ];
    }
}