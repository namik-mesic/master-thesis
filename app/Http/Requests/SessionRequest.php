<?php

namespace App\Http\Requests;

class SessionRequest extends Request
{
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users,email',
            'password' => 'required'
        ];
    }
}
