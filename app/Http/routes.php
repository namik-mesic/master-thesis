<?php

Route::resource('session', 'SessionController', [
    'only' => [
        'store',
        'destroy'
    ]
]);

Route::group([
    'prefix' => 'v1',
    'namespace' => 'V1',
    'middleware' => 'acl'
], function() {

    Route::resource('article', 'ArticleController', [
        'only' => [
            'index',
            'show',
            'store',
            'update'
        ]
    ]);

    Route::resource('user', 'UserController', [
        'only' => [
            'index',
            'show',
            'store',
            'update'
        ]
    ]);

    Route::resource('role', 'RoleController', [
        'only' => [
            'index',
            'show',
            'store',
            'update'
        ]
    ]);

    Route::resource('permission', 'PermissionController', [
        'only' => [
            'index',
            'show',
            'store',
            'update'
        ]
    ]);

    Route::resource('user.article', 'UserArticleController', [
        'only' => [
            'index'
        ]
    ]);

});
