<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class RedirectIfAuthenticated
{
    public function handle($request, Closure $next)
    {
        return \Auth::check()
            ? redirect('/home')
            : $next($request);
    }
}
