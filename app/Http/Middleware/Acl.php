<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Acl
{
    protected function failed()
    {
        return response('Unauthorized', 401);
    }

    public function handle(Request $request, Closure $next)
    {
        if (\Auth::guest())
            return $this->failed();

        $user = \Auth::user();

        $routeName = $request->route()->getUri();
        $routePieces = explode('/', $routeName);

        $finalPermission = strtolower($request->method()) . "/" . $routeName;
        $permissionSet = [];

        for ($i = 0; $i < count($routePieces); $i++)
            $permissionSet[] = $i == 0 ? $routePieces[$i] : $permissionSet[$i - 1] . "/" . $routePieces[$i];

        $permissionSet[] = $finalPermission;

        if (!$user->can($permissionSet))
            return $this->failed();

        return $next($request);
    }
}