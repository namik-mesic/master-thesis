<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class Authenticate
{
    public function handle(Request $request, Closure $next)
    {
        return \Auth::guest()
            ? response('Unauthorized.', 401)
            : $next($request);
    }
}
