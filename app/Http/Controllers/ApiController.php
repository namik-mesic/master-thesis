<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Routing\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Item as FractalItem;
use League\Fractal\Resource\Collection as FractalCollection;
use PointerBa\Bundle\Repository;
use Illuminate\Http\JsonResponse;

abstract class ApiController extends Controller
{
    /**
     * @var User
     *
     * currently active user
     */
    protected $user;

    /**
     * @var bool
     *
     * sets if a repo should be used
     */
    protected $noRepo = false;

    /**
     * @var null
     *
     * repo class name
     */
    protected $repoClass = null;

    /**
     * @var Repository|null
     *
     * derived class repository, null if does not exist
     */
    protected $repo = null;

    /**
     * @var Manager
     */
    protected $fractal;

    /**
     * Builds repository if exists based on Controller name or $repoClass if set
     */
    public function __construct()
    {
        $this->fractal = new Manager;

        if (!$this->noRepo)
        {
            if (!$this->repoClass)
            {
                $derivedClassName = 'App\\Repositories\\' . class_basename(get_called_class());

                $this->repoClass = str_replace('Controller', '', $derivedClassName) . 'Repository';
            }

            if (class_exists($this->repoClass))
                $this->repo = new $this->repoClass;
        }
    }

    /**
     * @param $message
     * @return JsonResponse
     */
    public function respondOk($message)
    {
        return response()->json($message);
    }

    /**
     * @param $message
     * @return JsonResponse
     */
    public function respondError($message)
    {
        return response()->json($message, 400);
    }

    public function respondItem(Model $model, $transformerClass)
    {
        return response()->json(
            $this->fractal->createData(new FractalItem($model, new $transformerClass))
                ->toArray()
        );
    }

    public function respondCollection(Collection $collection, $transformerClass)
    {
        return response()->json(
            $this->fractal->createData(new FractalCollection($collection, new $transformerClass))
                ->toArray()
        );
    }
}