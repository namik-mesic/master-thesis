<?php

namespace App\Http\Controllers;

use App\Http\Requests\SessionRequest;
use Illuminate\Http\Response;

class SessionController extends ApiController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  SessionRequest $request
     * @return Response
     */
    public function store(SessionRequest $request)
    {
        if (\Auth::user())
            return $this->respondError('You are already logged in');

        if (\Auth::attempt($request->only([
            'email',
            'password'
        ])))
            return $this->respondOk("You have been logged in");


        return $this->respondError('Invalid credentials');
    }

    /**
     * Remove the specified resource from storage.

     * @return Response
     */
    public function destroy()
    {
        if (!\Auth::user())
            return $this->respondError('You are not logged in');

        \Auth::logout();

        return $this->respondOk('You have been logged out');
    }
}