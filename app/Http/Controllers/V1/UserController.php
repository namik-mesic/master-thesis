<?php

namespace App\Http\Controllers\V1;

use App\Http\Requests\V1\StoreUserRequest;
use App\Repositories\UserRepository;
use App\User;
use App\Http\Controllers\ApiController;
use App\Http\Requests\V1\UserRequest;
use Illuminate\Http\Response;
use App\Transformers\UserTransformer;

class UserController extends ApiController
{
    /**
     * @var UserRepository
     */
    protected $repo;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->respondCollection(
            $this->repo->getAll(), UserTransformer::class
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreUserRequest $request
     * @return Response
     */
    public function store(StoreUserRequest $request)
    {
        $user = $this->repo->create($request->only([
            'name',
            'email',
            'password',
            'role_id'
        ]));

        return $this->respondItem(
            $user, UserTransformer::class
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @return Response
     */
    public function show(User $user)
    {
        return $this->respondItem(
            $user, UserTransformer::class
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserRequest $request
     * @param  User $user
     * @return Response
     */
    public function update(UserRequest $request, User $user)
    {
        $this->repo->update($user, $request->only([
            'name',
            'email',
            'password',
            'role_id'
        ]));

        return $this->respondItem(
            $user, UserTransformer::class
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return Response
     */
    public function destroy(User $user)
    {
        $this->repo->destroy($user);

        return $this->respondOk('Resource deleted');

    }
}