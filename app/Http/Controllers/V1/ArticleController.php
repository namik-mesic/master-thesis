<?php

namespace App\Http\Controllers\V1;

use App\Article;
use App\Http\Controllers\ApiController;
use App\Http\Requests\V1\ArticleRequest;
use App\Repositories\ArticleRepository;
use Illuminate\Http\Response;
use App\Transformers\ArticleTransformer;

class ArticleController extends ApiController
{
    /**
     * @var ArticleRepository
     */
    protected $repo;

    /**
     * Retrieve a collection of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $articles = $this->repo->getAll();

        return $this->respondCollection(
            $articles, ArticleTransformer::class
        );
    }

    /**
     * Store a newly created resource to storage.
     *
     * @param  ArticleRequest $request
     * @return Response
     */
    public function store(ArticleRequest $request)
    {
        $article = $this->repo->create($request);

        return $this->respondItem(
            $article, ArticleTransformer::class
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  Article $article
     * @return Response
     */
    public function show(Article $article)
    {
        return $this->respondItem(
            $article, ArticleTransformer::class
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ArticleRequest $request
     * @param  Article $article
     * @return Response
     */
    public function update(ArticleRequest $request, Article $article)
    {
        $this->repo->update($article, $request);

        return $this->respondItem(
            $article, ArticleTransformer::class
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Article $article
     * @return Response
     */
    public function destroy(Article $article)
    {
        $this->repo->destroy($article);

        return $this->respondOk('Resource deleted');
    }
}