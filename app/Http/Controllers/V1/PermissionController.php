<?php

namespace App\Http\Controllers\V1;

use App\Permission;
use App\Http\Controllers\ApiController;
use App\Http\Requests\V1\PermissionRequest;
use App\Repositories\PermissionRepository;
use Illuminate\Http\Response;
use App\Transformers\PermissionTransformer;

class PermissionController extends ApiController
{
    /**
     * @var PermissionRepository
     */
    protected $repo;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->respondCollection(
            $this->repo->getAll(), PermissionTransformer::class
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PermissionRequest $request
     * @return Response
     */
    public function store(PermissionRequest $request)
    {
        $permission = $this->repo->create($request);

        return $this->respondItem(
            $permission, PermissionTransformer::class
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  Permission $permission
     * @return Response
     */
    public function show(Permission $permission)
    {
        return $this->respondItem(
            $permission, PermissionTransformer::class
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PermissionRequest $request
     * @param  Permission $permission
     * @return Response
     */
    public function update(PermissionRequest $request, Permission $permission)
    {
        $this->repo->update($permission, $request);

        return $this->respondItem(
            $permission, PermissionTransformer::class
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Permission $permission
     * @return Response
     */
    public function destroy(Permission $permission)
    {
        $this->repo->destroy($permission);

        return $this->respondOk('Resource deleted');
    }
}