<?php

namespace App\Http\Controllers\V1;

use App\Repositories\RoleRepository;
use App\Role;
use App\Http\Controllers\ApiController;
use App\Http\Requests\V1\RoleRequest;
use Illuminate\Http\Response;
use App\Transformers\RoleTransformer;

class RoleController extends ApiController
{
    /**
     * @var RoleRepository
     */
    protected $repo;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->respondCollection(
            $this->repo->getAll(), RoleTransformer::class
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RoleRequest $request
     * @return Response
     */
    public function store(RoleRequest $request)
    {
        $role = $this->repo->create($request->only([
            'name',
            'display_name',
            'description',
            'permission_id'
        ]));

        return $this->respondItem(
            $role, RoleTransformer::class
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  Role $role
     * @return Response
     */
    public function show(Role $role)
    {
        return $this->respondItem(
            $role, RoleTransformer::class
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RoleRequest $request
     * @param  Role $role
     * @return Response
     */
    public function update(RoleRequest $request, Role $role)
    {
        $this->repo->update($role, $request->only([
            'name',
            'display_name',
            'description',
            'permission_id'
        ]));

        return $this->respondItem(
            $role, RoleTransformer::class
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Role $role
     * @return Response
     */
    public function destroy(Role $role)
    {
        $this->repo->destroy($role);

        return $this->respondOk('Resource deleted');
    }
}