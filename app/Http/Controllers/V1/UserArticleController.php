<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\ApiController;
use App\Repositories\ArticleRepository;
use App\User;
use Illuminate\Http\Response;
use App\Transformers\ArticleTransformer;

class UserArticleController extends ApiController
{
    /**
     * @var string
     */
    protected $repoClass = ArticleRepository::class;

    /**
     * @var ArticleRepository
     */
    protected $repo;

    /**
     * Display a listing of the resource.
     *
     * @param  User $user
     * @return Response
     */
    public function index(User $user)
    {
        return $this->respondCollection(
            $this->repo->getByUser($user), ArticleTransformer::class
        );
    }
}