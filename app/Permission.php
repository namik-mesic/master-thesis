<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PointerBa\Bundle\AuthoredTrait;
use PointerBa\Bundle\CacheObserver;
use Zizaco\Entrust\Contracts\EntrustPermissionInterface;
use Zizaco\Entrust\Traits\EntrustPermissionTrait;

class Permission extends Model implements EntrustPermissionInterface
{
    use EntrustPermissionTrait;
    use AuthoredTrait;

    protected $table = 'permissions';

    protected $fillable = [
        'name',
        'display_name',
        'description'
    ];

    public static function boot()
    {
        parent::boot();

        static::observe(CacheObserver::class);
    }
}