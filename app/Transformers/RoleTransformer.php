<?php

namespace App\Transformers;

use App\Role;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'author'
    ];

    public function transform(Role $role)
    {
        return [
            'id' => (int) $role->id,
            'name' => $role->name,
            'display_name' => $role->display_name,
            'description' => $role->description,
            'uri' => '/v1/role/' . $role->id,
            'permissions' => implode(',', $role->perms->lists('id')->toArray())
        ];
    }

    public function includeAuthor(Role $role)
    {
        return $this->item($role->author, new UserTransformer);
    }
}