<?php

namespace App\Transformers;

use App\Article;
use League\Fractal\TransformerAbstract;

class ArticleTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'author'
    ];

    public function transform(Article $article)
    {
        return [
            'id' => (int) $article->id,
            'title' => $article->title,
            'content' => $article->content,
            'image_uri' => $article->image->url(),
            'uri' => '/v1/article/' . $article->id
        ];
    }

    public function includeAuthor(Article $article)
    {
        return $this->item($article->author, new UserTransformer);
    }
}