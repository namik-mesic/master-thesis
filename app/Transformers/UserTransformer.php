<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;
use App\Transformers\RoleTransformer;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id' => (int) $user->id,
            'name' => $user->name,
            'uri' => '/v1/user/' . $user->id,
            'roles' => implode(',', $user->roles->lists('id')->toArray())
        ];
    }
}