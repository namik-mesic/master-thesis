<?php

namespace App\Transformers;

use App\Permission;
use League\Fractal\TransformerAbstract;

class PermissionTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'author'
    ];

    public function transform(Permission $permission)
    {
        return [
            'id' => (int) $permission->id,
            'name' => $permission->name,
            'display_name' => $permission->display_name,
            'description' => $permission->description,
            'uri' => '/v1/permission/' . $permission->id,
        ];
    }

    public function includeAuthor(Permission $permission)
    {
        return $this->item($permission->author, new UserTransformer);
    }
}