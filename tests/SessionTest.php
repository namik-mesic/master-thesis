<?php

/**
 * Class SessionTest
 *
 * Tests whether the session API works as intended
 */
class SessionTest extends TestCase
{
    /**
     * @test
     *
     * Tests if a dummy user may log in
     */
    public function logs_user_in()
    {
        // Act
        $this->login();

        // Assert
        $this->assertTrue((bool) Auth::user());
    }

    /**
     * @test
     *
     * Test if a logged in user can log out
     */
    public function logs_user_out()
    {
        // Arrange
        $this->login();

        // Act
        $this->forceLogout();

        // Assert
        $this->assertResponseOk();

        $this->assertFalse((bool) Auth::user());
    }
}