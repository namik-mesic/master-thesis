<?php

use App\Role;

/**
 * Class EntrustTest
 *
 * Tests whether the acl works as intended
 */
class EntrustTest extends TestCase
{
    /**
     * @test
     *
     * Users that are not logged in may not manage any protected content
     */
    public function prohibits_content_management_for_unauthorized_users()
    {
        // Arrange
        $this->forceLogout();

        $data = [
            'title' => 'TestCase title',
            'content' => 'TestCase content'
        ];

        // Act
        $this->post('v1/article', $data);

        // Assert
        $this->assertResponseStatus(401);
    }

    /**
     * @test
     *
     * Makes sure that a role that has no permission for
     */
    public function prohibits_custom_role_from_unauthorized_resources()
    {
        // Arrange
        $this->user->attachRole(factory(Role::class)->make());

        $this->login();

        // Act
        $this->get('v1/article');

        // Assert
        $this->assertResponseStatus(401);
    }

    public function allows_admin_to_view_resources()
    {
        // Arrange
        $this->adminLogin();

        // Act
        $this->get('v1/article');

        // Assert
        $this->assertResponseOk();
    }
}