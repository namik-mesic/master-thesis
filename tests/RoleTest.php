<?php

use App\Role;

/**
 * Class RoleTest
 *
 * Tests if the v1 role API works as intended
 */
class RoleTest extends TestCase
{
    public function makeDummyRole()
    {
        return factory(Role::class)->make([
            'name' => 'UniqueRole' . microtime()
        ]);
    }

    /**
     * @test
     *
     * Tests if a logged and authorized user may post a role
     */
    public function can_post_roles_when_logged_in_and_authorized()
    {
        // Arrange
        $this->adminLogin();

        $data = [
            'name' => 'UniqueRole' . microtime()
        ];

        // Act
        $this->post('v1/role', $data);

        // Assert
        $this->assertResponseOk();
    }

    /**
     * @test
     *
     * Tests if a logged and authorized user may edit an existing role
     */
    public function can_modify_existing_roles_when_logged_in_and_authorized()
    {
        // Arrange
        $this->adminLogin();

        $role = $this->makeDummyRole();
        $role->save();

        $data = [
            'name' => 'TestCase' . $role->name
        ];

        // Act
        $this->put('v1/role/' . $role->id, $data);

        // Assert
        $this->assertResponseOk();
    }

    /**
     * @test
     *
     * Tests if a logged and authorized user may delete an existing role
     */
    public function can_delete_existing_roles_when_logged_in_and_authorized()
    {
        // Arrange
        $this->adminLogin();

        $role = $this->makeDummyRole();
        $role->save();

        // Act
        $this->delete('v1/role/' . $role->id);

        // Assert
        $this->assertResponseOk();

        $this->assertFalse((bool) Role::find($role->id));
    }

    /**
     * @test
     *
     * Tests if a logged and authorized user may fetch an existing role
     */
    public function can_fetch_a_role_when_logged_in_and_authorized()
    {
        // Arrange
        $this->adminLogin();

        $role = $this->makeDummyRole();
        $role->save();

        // Act
        $this->get('v1/role/' . $role->id);

        // Assert
        $this->assertResponseOk();

        $this->seeJson([
            'name' => $role->name
        ]);
    }
}