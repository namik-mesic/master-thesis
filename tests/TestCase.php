<?php

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class TestCase
 *
 * Base test class
 */
class TestCase extends BaseTestCase
{
    use DatabaseTransactions;

    /**
     * @var string
     *
     * Base site url
     */
    protected $baseUrl = 'http://localhost:8000';

    /**
     * @var User
     */
    protected $user;

    /**
     * @var User
     */
    protected $admin;

    /**
     * Makes a dummy user and sets up the TestCase
     */
    public function __construct()
    {
        parent::__construct();
        parent::setUp();

        $this->user = $this->makeDummyUser();
        $this->admin = User::getDefaultAdmin();
    }

    /**
     * @return App
     *
     * Crafts the virtual application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    /**
     * Attempts to log in as the default administrator user
     */
    public function adminLogin()
    {
        \Auth::logout();

        $this->post('session', [
            'email' => $this->admin->email,
            'password' => 'password'
        ]);

        $this->assertResponseOk();
    }

    /**
     * Forces removal of current user
     */
    public function forceLogout()
    {
        \Auth::logout();
    }

    /**
     * @param array|null $credentials
     *
     * Attempts to make a new session through the API
     */
    public function login(array $credentials = null)
    {
        \Auth::logout();

        if (is_null($credentials))
            $credentials = [
                'email' => $this->user->email,
                'password' => 'password'
            ];

        $this->post('session', $credentials);

        $this->assertResponseOk();
    }

    /**
     * Attempts to destroy the current session through the API
     */
    public function attemptLogout()
    {
        $this->delete('session');
    }

    /**
     * @param string $password
     * @return User
     *
     * Creates a dummy user
     */
    public function makeDummyUser($password = 'password')
    {
        return factory(User::class)->create([
            'password' => $password
        ]);
    }
}
