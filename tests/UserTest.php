<?php

use App\User;

/**
 * Class UserTest
 *
 * Tests if the v1 user API works as intended
 */
class UserTest extends TestCase
{
    /**
     * @test
     *
     * Tests if a logged and authorized user may post a user
     */
    public function can_post_users_when_logged_in_and_authorized()
    {
        // Arrange
        $this->adminLogin();

        $data = [
            'name' => 'TestCase name',
            'email' => 'testcase@gmail.com',
            'password' => 'password'
        ];

        // Act
        $this->post('v1/user', $data);

        // Assert
        $this->seeInDatabase('users', [
            'email' => 'testcase@gmail.com'
        ]);
    }

    /**
     * @test
     *
     * Tests if a logged and authorized user may edit an existing user
     */
    public function can_modify_existing_users_when_logged_in_and_authorized()
    {
        // Arrange
        $this->adminLogin();

        $user = $this->makeDummyUser();
        $user->save();

        $data = [
            'email' => 'TestCase' . $user->email,
            'name' => 'TestCase' . $user->name
        ];

        // Act
        $this->put('v1/user/' . $user->id, $data);

        // Assert
        $this->assertResponseOk();
    }

    /**
     * @test
     *
     * Tests if a logged and authorized user may delete an existing user
     */
    public function can_delete_existing_users_when_logged_in_and_authorized()
    {
        // Arrange
        $this->adminLogin();

        $user = $this->makeDummyUser();
        $user->save();

        // Act
        $this->delete('v1/user/' . $user->id);

        // Assert
        $this->assertResponseOk();

        $this->assertFalse((bool) User::find($user->id));
    }

    /**
     * @test
     *
     * Tests if a logged and authorized user may fetch an existing user
     */
    public function can_fetch_a_user_when_logged_in_and_authorized()
    {
        // Arrange
        $this->adminLogin();

        $user = $this->makeDummyUser();

        // Act
        $this->get('v1/user/' . $user->id);

        // Assert
        $this->assertResponseOk();

        $this->seeJson([
            'email' => $user->email
        ]);
    }
}