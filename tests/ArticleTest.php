<?php

use App\Article;

/**
 * Class ArticleTest
 *
 * Tests if the v1 article API works as intended
 */
class ArticleTest extends TestCase
{
    /**
     * @test
     *
     * Tests if a logged and authorized user may post a article
     */
    public function can_post_articles_when_logged_in_and_authorized()
    {
        // Arrange
        $this->adminLogin();

        $data = [
            'title' => 'TestCase title',
            'content' => 'TestCase content'
        ];

        // Act
        $this->post('v1/article', $data);

        // Assert
        $this->seeInDatabase('articles', $data);
    }

    /**
     * @test
     *
     * Tests if a logged and authorized user may edit an existing article
     */
    public function can_modify_existing_articles_when_logged_in_and_authorized()
    {
        // Arrange
        $this->adminLogin();

        $article = factory(Article::class)->create();

        $data = [
            'title' => 'TestCase' . $article->title,
            'content' => 'TestCase' . $article->content,
            '_method' => 'PATCH'
        ];

        // Act
        $this->post('v1/article/' . $article->id, $data);

        // Assert
        $this->assertResponseOk();

        $this->seeInDatabase('articles', $data);
    }

    /**
     * @test
     *
     * Tests if a logged and authorized user may delete an existing article
     */
    public function can_delete_existing_articles_when_logged_in_and_authorized()
    {
        // Arrange
        $this->adminLogin();

        $article = factory(Article::class)->create();

        // Act
        $this->post('v1/article/' . $article->id, [
            '_method' => 'DELETE'
        ]);

        // Assert
        $this->assertResponseOk();

        $this->assertFalse((bool) Article::find($article->id));
    }

    /**
     * @test
     *
     * Tests if a logged and authorized user may fetch an existing article
     */
    public function can_fetch_an_article_when_logged_in_and_authorized()
    {
        // Arrange
        $this->adminLogin();
        $article = factory(Article::class)->make();
        $article->save();

        // Act
        $this->get('v1/article/' . $article->id);

        // Assert
        $this->assertResponseOk();

        $this->seeJson([
            'title' => $article->title
        ]);
    }
}